These are Cordova resources. You can replace icon.png and splash.png and run
`ionic cordova resources` to generate custom icons and splash screens for your
app. See `ionic cordova resources --help` for details.

Cordova reference documentation:

- Icons: https://cordova.apache.org/docs/en/latest/config_ref/images.html
- Splash Screens: https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-splashscreen/

# To enable CORS 
1. Goto https://cors-anywhere.herokuapp.com/corsdemo and enable access

# To Run/Build App
1. ionic build
2. npx cap add <platform>
3. For icon and splash screen refer https://capacitorjs.com/docs/guides/splash-screens-and-icons
4. npx cap copy <platform>
5. npx cap sync <platform>
6. npx cap open <platform>

P.S: Step2 will run for the first time when you setup android platform in you machine. Step3 will run only for the initial project setup that is already done, so no need to run it(OR run it when you want to change icon and splash screen)

# To Run App live reload
1. ionic cap run <platform> -l --external
https://capacitorjs.com/docs/guides/live-reload

# For Android setup need few things in AndroidMnifest.xml
1. Goto android/app/src/main/AndroidMnifest.xml
2. Add android:usesCleartextTraffic="true" in application for using unsecured http request. Like: <application android:hardwareAccelerated="true" android:icon="@mipmap/ic_launcher" android:label="@string/app_name" android:largeHeap="true" android:requestLegacyExternalStorage="true" android:supportsRtl="true" android:usesCleartextTraffic="true">
3. For file write permission add  <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />


# If you getting this error (android-support-v4-content-fileprovider-not-found)
1.change the file of fileProvider.java only change the public class extend androidx.core.content.FileProvider 
2)url(https://stackoverflow.com/questions/48534293/android-support-v4-content-fileprovider-not-found) solution file. 
