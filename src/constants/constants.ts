export const TASK_CATEGORIES = {
  PURCHASING: 'Purchasing',
  PURCHASE_REQUISITIONS: 'Purchase Requisitions',
  INVOICE: 'Invoice',
};
export const TABS_TYPE = {
  INFO: 'Info',
  HISTORY: 'History',
  ATTACHMENT: 'Attachment',
  TASK: 'Task',
  FORWARD: 'Forward'
};
export const REG_EX = {
  PO_REGEX: /[0-9]{1,}/,
  INVOICE_REGEX: /INV_[0-9]{1,}/i
};
