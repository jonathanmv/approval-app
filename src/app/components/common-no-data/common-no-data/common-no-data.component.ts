import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-common-no-data',
  templateUrl: './common-no-data.component.html',
  styleUrls: ['./common-no-data.component.scss'],
})
export class CommonNoDataComponent implements OnInit {
  @Input() text: any;

  constructor() { }

  ngOnInit() { }
}
