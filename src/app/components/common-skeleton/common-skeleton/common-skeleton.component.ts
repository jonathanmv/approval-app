import { Component, Input, OnInit } from '@angular/core';
import { TABS_TYPE } from 'src/constants/constants';

@Component({
  selector: 'app-common-skeleton',
  templateUrl: './common-skeleton.component.html',
  styleUrls: ['./common-skeleton.component.scss'],
})
export class CommonSkeletonComponent implements OnInit {
  private defaultItem = new Array(8);
  @Input() id;
/**
 * @param defaultItem provided the default card
 * @param id
 */
  readonly TABS_TYPE = TABS_TYPE;
  constructor() { }

  ngOnInit() { }
}
