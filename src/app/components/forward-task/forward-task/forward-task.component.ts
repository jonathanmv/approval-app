import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { ApiService } from 'src/app/services/api-service/api.service';
import { CommonUiService } from 'src/app/services/common-ui/common-ui.service';
import { TABS_TYPE } from 'src/constants/constants';

@Component({
  selector: 'app-forward-task',
  templateUrl: './forward-task.component.html',
  styleUrls: ['./forward-task.component.scss'],
})
export class ForwardTaskComponent implements OnInit {

  @Output() updated = new EventEmitter<string>();
  name: any;
  filterTerm: any;
  public users: any;
  private body: { tasks: {}; action: {}; comment: {}; identities: {} };
  contentLoaded = false;
  readonly TABS_TYPE = TABS_TYPE;
  forwardUserInfoSubscription: Subscription;
  DataNotFoundMessage = this.translate.instant('NO_DATA.USER');

  /**
   *
   * @param modalCtrl
   * @param apiService
   * @param commonUiService
   */
  constructor(
    private modalCtrl: ModalController,
    public apiService: ApiService,
    public commonUiService: CommonUiService,
    private translate: TranslateService
  ) { }
  cancelForwardModal() {
    this.modalCtrl.dismiss();
  }

  async getUsers() {
    //Call Users API for Forward
    this.apiService.getUsers().then((data) => {
      console.log('Data', data);
      this.contentLoaded = true;
    }).catch((error) => {
      this.contentLoaded = true;
      console.error(error);
    });
  }

  ngOnInit() {
    this.forwardUserInfoSubscription = this.apiService.forwardUserInfoChanged$.subscribe((list) => {
      this.users = list;
    });
    if (this.users && this.users.length > 0) {
      this.contentLoaded = true;
    } else {
      this.getUsers();
    };
  }
  /**
   *
   * @param userId
   */
  selectedUser(userId) {
    this.modalCtrl.dismiss(userId);
  }

  /**
   *
   * @param ev
   */
  fetchSearchInputValue(ev: any) {
    this.filterTerm = ev.target.value;
  }

  ngOnDestroy(): void {
    if (this.forwardUserInfoSubscription) {
      this.forwardUserInfoSubscription.unsubscribe();
    }
  }

}
