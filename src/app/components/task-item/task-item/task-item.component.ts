import { Component, Input, OnInit } from '@angular/core';
import { TASK_CATEGORIES } from 'src/constants/constants';
import * as _ from 'lodash';
import { SharedService } from 'src/app/services/shared-services/shared-services.service';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss'],
})
export class TaskItemComponent implements OnInit {
  @Input() items: any;
  @Input() category: any;
  readonly TASK_CATEGORIES = TASK_CATEGORIES;
  filteredTaskItems: any = [];
  constructor(
    private sharedService: SharedService
  ) { }

  ngOnInit() {
    this.groupingOfTaskItems();
  }

  /**
   * Grouping of Task Items Based on Certain fields
   */
  groupingOfTaskItems() {
    let groupedTaskItemsArray = this.sharedService.groupByTaskItems(this.items, (item) => {
      if (this.category === TASK_CATEGORIES.INVOICE) {
        return [item.ItemDescription, item.UnitPrice, item.Quantity, item.ChargeAccount];
      } else if (this.category === TASK_CATEGORIES.PURCHASE_REQUISITIONS || this.category === TASK_CATEGORIES.PURCHASING) {
        return [item.ItemNumber, item.ItemDescription, item.UnitPrice, item.Quantity, item.LineTotal];
      }
    });
    console.log("groupedTaskItemsArray", groupedTaskItemsArray);

    // Fetch first element of each array exist in groupedTaskItemsArray
    let finalizedTaskItemsArray = groupedTaskItemsArray.map((elem) => {
      return elem[0];
    });

    console.log("finalizedTaskItemsArray", finalizedTaskItemsArray);
    this.filteredTaskItems = finalizedTaskItemsArray;
  }

}
