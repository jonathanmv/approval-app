import { Component, OnInit, Input } from '@angular/core';
import { TASK_CATEGORIES } from 'src/constants/constants';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-details-header',
  templateUrl: './task-details-header.component.html',
  styleUrls: ['./task-details-header.component.scss'],
})
export class TaskDetailsHeaderComponent implements OnInit {
  @Input('taskInformation') taskInformation: any;
  readonly TASK_CATEGORIES = TASK_CATEGORIES;
  category: any;

  constructor(
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.category = this.activatedRoute.snapshot.params['category'];
  }

}
