import { Component, Input, OnInit } from '@angular/core';
import { TASK_CATEGORIES } from 'src/constants/constants';

@Component({
  selector: 'app-taskdata',
  templateUrl: './taskdata.component.html',
  styleUrls: ['./taskdata.component.scss'],
})
export class TaskdataComponent implements OnInit {
  @Input() listTasks: any = [];
  readonly TASK_CATEGORIES = TASK_CATEGORIES;
  constructor() {

  }

  ngOnInit() {
    console.log(this.listTasks, "data-component");
  }

}
