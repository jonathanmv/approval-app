import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { TabsComponent } from './all-tabs/tabs/tabs.component';
import { CommonNoDataComponent } from './common-no-data/common-no-data/common-no-data.component';
import { CommonSearchbarComponent } from './common-searchbar/common-searchbar/common-searchbar/common-searchbar.component';
import { CommonSkeletonComponent } from './common-skeleton/common-skeleton/common-skeleton.component';
import { ForwardTaskComponent } from './forward-task/forward-task/forward-task.component';
import { PopoverComponent } from './popover/popover/popover.component';
import { TabComponent } from './tab/tab.component';
import { TaskDetailsHeaderComponent } from './task-details-header/task-details-header.component';
import { TaskItemComponent } from './task-item/task-item/task-item.component';
import { TaskdataComponent } from './taskcomponent/taskdata/taskdata.component';
@NgModule({
    declarations: [
        CommonSearchbarComponent,
        CommonSkeletonComponent,
        CommonNoDataComponent,
        ForwardTaskComponent,
        TaskItemComponent,
        TabsComponent,
        TaskDetailsHeaderComponent,
        TaskdataComponent,
        TabComponent,
        PopoverComponent
    ],
    imports: [
        FormsModule,
        CommonModule,
        Ng2SearchPipeModule,
        IonicModule,
        TranslateModule
    ],
    exports: [
        CommonSearchbarComponent,
        CommonSkeletonComponent,
        CommonNoDataComponent,
        ForwardTaskComponent,
        TaskItemComponent,
        TabsComponent,
        TaskDetailsHeaderComponent,
        TaskdataComponent,
        TabComponent,
        PopoverComponent

    ]
})
export class SharedCompenentsModule { }
