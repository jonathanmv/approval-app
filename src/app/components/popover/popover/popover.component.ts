import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { LanguageServices } from 'src/app/services/language-services/language-services.service';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent implements OnInit {
  languages: any[]= [];
  selectedLanguage = '';
  constructor(private popoverCtrl: PopoverController,
    private languageService: LanguageServices) { }

  ngOnInit() {
    this.languages=this.languageService.sendLanguageTextValue();
    this.selectedLanguage = this.languageService.selected;
  }

  /**
   *
   * @param lng take the selectlanguage
   */

  selectLanguage(lng) {
    this.languageService.setLanguageFromLocalStroage(lng);
    this.popoverCtrl.dismiss();
  }
}
