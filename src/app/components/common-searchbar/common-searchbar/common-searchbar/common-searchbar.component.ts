import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-common-searchbar',
  templateUrl: './common-searchbar.component.html',
  styleUrls: ['./common-searchbar.component.scss'],
})
export class CommonSearchbarComponent implements OnInit {
  @Output() searchItems = new EventEmitter();
  @Input() searchInput;
  @Input() placeHolder;
  constructor() { }

  ngOnInit() { }

  /**
   * Search entered element
   * @param event 
   */
  searchEnteredElement(event) {
    this.searchItems.emit(event);
  }
}
