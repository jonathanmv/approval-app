import { Injectable } from '@angular/core';
import { Http, HttpOptions } from '@capacitor-community/http';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceProvider {

  constructor() { }

  async doGet(url, option?){
    const options: HttpOptions = {
      method: 'GET',
      url: url, 
      headers: {
        "Authorization": "Basic UkFKLmNsZDE6d2VsY29tZTEyMw=="
      },
    };

    return await Http.request(option || options).then(res => {
      if(res && res.data){
        return res.data
      }
    });
  }

  async doPut(options){
    return await Http.request(options).then(res => {
      if(res && res.data){
        return res.data
      }
    });
  }
}
