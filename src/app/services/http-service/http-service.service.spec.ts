import { TestBed } from '@angular/core/testing';

import { HttpServiceProvider } from './http-service.service';

describe('HttpServiceProvider', () => {
  let service: HttpServiceProvider;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HttpServiceProvider);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
