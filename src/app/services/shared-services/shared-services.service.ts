import { Injectable, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { TASK_CATEGORIES,REG_EX } from 'src/constants/constants';

@Injectable({
  providedIn: 'root',
})
export class SharedService {
  constructor() { }

  /**
   * Extract CategoryNumber from Title depending on TaskCategory
   * @param category
   * @param str
   * @returns
   */
  extractCategoryNumberFromTitle(category, str) {
    if (category === TASK_CATEGORIES.PURCHASING ||
      category === TASK_CATEGORIES.PURCHASE_REQUISITIONS) {

      let extractNumber = REG_EX.PO_REGEX.exec(str);
      if (extractNumber && extractNumber.length > 0 && extractNumber[0].match(REG_EX.PO_REGEX)) {
        return extractNumber[0];
      } else {
        return '';
      }
    }
    else if(category === TASK_CATEGORIES.INVOICE) {

      let extractNumber = REG_EX.INVOICE_REGEX.exec(str);
      if (extractNumber && extractNumber.length > 0 && extractNumber[0].match(REG_EX.INVOICE_REGEX)) {
        return extractNumber[0];
      } else {
        return '';
      }
    }
  }

  /**
   * GroupBy Task Items based on certain fields
   * @param array
   * @param f
   * @returns
   */
  groupByTaskItems(array, f) {
    let groups = {};
    array.forEach(function (o) {
      var group = JSON.stringify(f(o));
      groups[group] = groups[group] || [];
      groups[group].push(o);
    });
    return Object.keys(groups).map(function (group) {
      return groups[group];
    });
  };
}
