import { TestBed } from '@angular/core/testing';

import { AuthServiceProvider } from './auth-service.service';

describe('AuthServiceProvider', () => {
  let service: AuthServiceProvider;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthServiceProvider);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
