import { Injectable } from '@angular/core';
import { StorageProviderService } from '../storage-provider/storage-provider.service';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceProvider {

  constructor(
    private storage: StorageProviderService
  ) { }

  async getAuthStatus(){
    return await this.storage.getData('isUserAuthorized');
  }

  async setAuthStatus(value){
    await this.storage.setData('isUserAuthorized', value);
  }
}
