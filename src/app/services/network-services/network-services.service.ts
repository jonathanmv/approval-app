import { Injectable } from '@angular/core';

import { Network } from '@ionic-native/network/ngx';
import { Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { CommonUiService } from '../common-ui/common-ui.service';
@Injectable({
  providedIn: 'root'
})
export class NetworkServices {

  constructor(private network: Network,
    private ui: CommonUiService,
    private plt: Platform,
    private translate: TranslateService) {


    this.plt.ready().then(() => {

      this.networkDetector();

    });
  }



  /**
   * networkDetector is use for detect the network is online or offline
   */
  networkDetector() {

    window.addEventListener('offline', () => {
      this.ui.presenErrortToast(this.translate.instant('TOSTER_MESSAGE.ERROR_MESSAGE'));

    });
    window.addEventListener('online', () => {
      this.ui.presentSuccessToast(this.translate.instant('TOSTER_MESSAGE.SUCCESS_MESSAGE'));

    });
  }
}
