import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AlertController, isPlatform, LoadingController } from '@ionic/angular';
import { Http, HttpOptions } from '@capacitor-community/http';
import { BehaviorSubject, from, Observable } from 'rxjs';
import { HttpServiceProvider } from '../http-service/http-service.service';
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public apiKey: any;
  public taskId: number;
  private comment = '';

  private forwardUserInfoSubject$: BehaviorSubject<[]> = new BehaviorSubject<
    []
  >([]);
  forwardUserInfoChanged$: Observable<[]> =
    this.forwardUserInfoSubject$.asObservable();

  private authKey = 'Basic UkFKLmNsZDE6d2VsY29tZTEyMw==';
  private orcaleUrl =
    'https://edrx-dev1.fa.us2.oraclecloud.com/bpm/api/4.0/tasks';

  constructor(
    private http: HttpClient,
    private httpService: HttpServiceProvider
  ) {}

  getRequest() {
    // Click to enable cors
    // https://cors-anywhere.herokuapp.com/corsdemo
    const headers = new HttpHeaders().set('Authorization', this.authKey);

    const url = this.orcaleUrl;
    
    if (isPlatform('capacitor')) {
      return from(this.httpService.doGet(url+ '?limit=500'));
    } else {
      return this.http.get(`https://cors-anywhere.herokuapp.com/${url}?limit=500`, {
        headers: headers,
      });
    }
  }

  getTaskInformationById(taskId) {
    const headers = new HttpHeaders().set('Authorization', this.authKey);

    const url = `https://testnode.propelapps.com/CLD/SOAP/21D/getAllApprovalDetails/${taskId}`;
    if (isPlatform('capacitor')) {
      //TODO: Capacitor http not working for unsecured http request, need to check Error: IOException
      // return from(this.httpService.doGet(url));
      return this.http.get(url);
    } else {
      return this.http.get(url);
    }
  }

  updateTaskInformationById(body) {
    const headers = new HttpHeaders()
      .set('Authorization', this.authKey)
      .set('Content-Type', 'application/json');

    console.log('Put Body', JSON.stringify(body));

    // const url = "https://ejdh-dev1.fa.em2.oraclecloud.com:443/bpm/api/4.0/tasks";
    const url =
      'https://edrx-dev1.fa.us2.oraclecloud.com:443/bpm/api/4.0/tasks';
    if (isPlatform('capacitor')) {
      //TODO: Write code for mobile app
      const options: HttpOptions = {
        method: 'PUT',
        url: url,
        headers: {
          Authorization: this.authKey,
          'Content-Type': 'application/json',
        },
        data: body,
      };

      // return from(Http.put(options));
      return from(this.httpService.doPut(options));
    } else {
      console.log('__APIPUT__', body);
      return this.http.put(`https://cors-anywhere.herokuapp.com/${url}`, body, {
        headers: headers,
      });
    }
  }

  public getJSON() {
    return this.http.get('./assets/test-task/test-task.json');
  }

  setComment(commentTaskList) {
    this.comment = commentTaskList;
  }

  getComment() {
    return this.comment;
  }

  getUser() {
    // const url = `https://testnode.propelapps.com/CLD/SOAP/21D/getAllUserDetails/300000010164856/""`;
    const url = `https://testnode.propelapps.com/CLD/SOAP/21D/getAllUserDetails/300000093570409/""`;

    if (isPlatform('capacitor')) {
      //TODO: Capacitor http not working for unsecured http request, need to check Error: IOException
      // return from(this.httpService.doGet(url));
      return this.http.get(url);
    } else {
      return this.http.get(url);
    }
  }

  getTaskHistory(taskId) {
    const headers = new HttpHeaders()
      .set('Authorization', this.authKey)
      .set('Content-Type', 'application/json');
    const url = `${this.orcaleUrl}/${taskId}/history`;
    if (isPlatform('capacitor')) {
      //TODO: Write code for mobile app
      return from(this.httpService.doGet(url));
    } else {
      return this.http.get(`https://cors-anywhere.herokuapp.com/${url}`, {
        headers: headers,
      });
    }
  }

  getAttachments(taskId) {
    const headers = new HttpHeaders().set('Authorization', this.authKey);
    const url = `${this.orcaleUrl}/${taskId}/attachments`;
    if (isPlatform('capacitor')) {
      //TODO: Write code for mobile app
      return from(this.httpService.doGet(url));
    } else {
      return this.http.get(`https://cors-anywhere.herokuapp.com/${url}`, {
        headers: headers,
      });
    }
  }

  downloadAttachments(url) {
    const headers = new HttpHeaders().set('Authorization', this.authKey);
    if (isPlatform('capacitor')) {
      //TODO: Write code for mobile app
      const options = {
        method: 'GET',
        url: url,
        headers: {
          Authorization: this.authKey,
        },
        responseType: 'blob',
        reportProgress: true,
        observe: 'events',
      };
      return from(this.httpService.doGet(url, options));
    } else {
      return this.http.get(`https://cors-anywhere.herokuapp.com/${url}`, {
        responseType: 'blob',
        reportProgress: true,
        observe: 'events',
        headers: headers,
      });
    }
  }

  async getUsers() {
    return await this.getUser()
      .toPromise()
      .then(
        (res) => {
          if (res && res['AllUserDetails']) {
            let list = res['AllUserDetails'].filter(
              (el) => el.UserId != '300000010440870'
            );
            this.setForwardUserInfo(list);
          }
        },
        (error) => {
          console.log('Error', error);
        }
      );
  }

  setForwardUserInfo(forwardUser) {
    this.forwardUserInfoSubject$.next(forwardUser);
  }
}
