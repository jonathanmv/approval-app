import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';

import { StorageProviderService } from '../storage-provider/storage-provider.service';

/**
 * @param LNG_KEY key
 */
const LNG_KEY = 'LNG_KEY';
@Injectable({
  providedIn: 'root',
})
export class LanguageServices {
  selected = '';
  constructor(
    private translate: TranslateService,
    private storage: StorageProviderService,

  ) { }

  /**
   * @param selected use to show the selected value in UI
   * @param val
   * @param LNG_KEY
   * this function is set the default brower language
   */
  setInitialAppLanguage() {
    const language = this.translate.getBrowserLang();
    this.translate.setDefaultLang(language);
    this.storage.getData(LNG_KEY).then(val => {

      if (val) {
        this.setLanguageFromLocalStroage(val);

        this.selected = val;
      }
    });
  }

  /**
   *
   * @returns the key:text and value this is use the popovercomponent to show the language name
   */
  sendLanguageTextValue() {
    return [
      { text: 'English', value: 'en' },
      {text:'German', value:'de'}

    ];
  }
/**
 *
 * @param lng
 * this function use to set the language of the storage chage the default language
 */
  setLanguageFromLocalStroage(lng) {
    this.translate.use(lng);
    this.selected = lng;
    this.storage.setData(LNG_KEY, lng);
  }
}
