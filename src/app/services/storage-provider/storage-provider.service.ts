import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
@Injectable({
  providedIn: 'root'
})
export class StorageProviderService {

  constructor(
    private storage: Storage
  ) { }

  async setData(key, value){
    await this.storage.set(key , value);
  }

  async getData(key){
    const value = await this.storage.get(key);
    return value;
  }
}
