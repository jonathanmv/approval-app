import { Injectable } from '@angular/core';
import { AlertController, isPlatform, LoadingController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
@Injectable({
  providedIn: 'root'
})
export class CommonUiService {

  constructor(
    public loadingController: LoadingController,
    public toastController: ToastController,
    private translate: TranslateService
  ) { }

  /**
   *  Show the loader for infinite time
   */
  showLoader() {
    this.loadingController.create({
      message: this.translate.instant('LOADING_MESSAGE.HEADER')
    }).then((res) => {
      res.present();
    });
  }

  /**
   * Hide the loader if already created otherwise return error
   */
  hideLoader() {
    this.loadingController.dismiss().then((res) => {
      console.log('Loading dismissed!', res);
    }).catch((error) => {
      console.log('error', error);
    });
  }

  /**
   * Toast for Success message
   * @param message
   */
  async presentSuccessToast(message: string = 'Transaction is succesful') {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color: 'success'
    });
    toast.present();
  }

  /**
   * Toast for Error message
   * @param message
   */
  async presenErrortToast(message: string = 'Something went wrong') {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
      color: 'danger',
    });
    toast.present();
  }

}
