import { TestBed } from '@angular/core/testing';

import { TaskDetailsDataService } from './task-details-data.service';

describe('TaskDetailsDataService', () => {
  let service: TaskDetailsDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaskDetailsDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
