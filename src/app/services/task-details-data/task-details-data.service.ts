import { Injectable } from '@angular/core';

@Injectable()

export class TaskDetailsDataService {
  private currentTaskInfo;
  private currentTaskNumber;
  private currentTaskCategory;

  /**
   * Set CurrentTask Number
   * @param currentTask 
   */
  setCurrentTaskNumber(currentTaskNumber) {
    this.currentTaskNumber = currentTaskNumber;
  }

  /**
   * Get CurrentTask Number
   * @returns 
   */
  getCurrentTaskNumber() {
    return this.currentTaskNumber;
  }

  /**
* Set CurrentTask Category
* @param currentTaskCategory
*/
  setCurrentTaskCategory(currentTaskCategory) {
    this.currentTaskCategory = currentTaskCategory;
  }

  /**
   * Get CurrentTask Info
   * @returns 
   */
  getCurrentTaskCategory() {
    return this.currentTaskCategory;
  }

  /**
   * Set CurrentTask Info
   * @param currentTask 
   */
  setCurrentTaskInfo(currentTask) {
    this.currentTaskInfo = currentTask;
  }

  /**
   * Get CurrentTask Info
   * @returns 
   */
  getCurrentTaskInfo() {
    return this.currentTaskInfo;
  }
}
