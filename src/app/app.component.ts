import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Platform } from '@ionic/angular';
import { LanguageServices } from './services/language-services/language-services.service';
import { Network } from '@ionic-native/network/ngx';
import { CommonUiService } from './services/common-ui/common-ui.service';
import { NetworkServices } from './services/network-services/network-services.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})

export class AppComponent implements OnInit {
  public list = [];

  constructor(
    private storage: Storage,
    private platform: Platform,
    private language: LanguageServices,

    private network: NetworkServices

  ) {
  }

  ngOnInit() {
    this.intializeApp();


  }

  async intializeApp() {

    this.platform.ready().then(async () => {
      // If using a custom driver:
      await this.storage.create();
      // Use for Language translate method
      /**
       *setInitialAppLanguage method is declar inside the languageservices
       */
      this.language.setInitialAppLanguage();


    });
  }
}
