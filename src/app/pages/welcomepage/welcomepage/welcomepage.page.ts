import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthServiceProvider } from 'src/app/services/auth-service/auth-service.service';

@Component({
  selector: 'app-welcomepage',
  templateUrl: './welcomepage.page.html',
  styleUrls: ['./welcomepage.page.scss'],
})
export class WelcomepagePage implements OnInit {

  constructor(
    public navCtrl: NavController,

    private authService: AuthServiceProvider
  ) { }

  ngOnInit() {
  }
  /**
   *
   * @returns
   * go to signIn page
   */
  async signIn() {

    this.navCtrl.navigateRoot('/login');
    await this.authService.setAuthStatus(true);
    return;
  }
}
