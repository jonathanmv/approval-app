import { Router } from '@angular/router';
/* eslint-disable*/
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api-service/api.service';
import {
  ToastController,
  AlertController,
  NavController,
} from '@ionic/angular';
import { TaskDetailsPage } from '../task-details/task-details.page';
import { ModalController, isPlatform } from '@ionic/angular';
import { CommonUiService } from '../../services/common-ui/common-ui.service';
import { ForwardTaskComponent } from '../../components/forward-task/forward-task/forward-task.component';
import { ActivatedRoute } from '@angular/router';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { TASK_CATEGORIES } from 'src/constants/constants';

@Component({
  selector: 'app-action-buttons',
  templateUrl: './action-buttons.component.html',
  styleUrls: ['./action-buttons.component.scss'],
})
export class ActionButtonsComponent implements OnInit {
  private currentTask: {};
  private body: { tasks: {}; action: {}; comment: {}; identities: {} };

  constructor(
    private router: Router,
    private apiService: ApiService,
    private toastController: ToastController,
    private alertController: AlertController,
    private taskDetailsPage: TaskDetailsPage,
    public modalController: ModalController,
    public commonUiService: CommonUiService,
    private navController: NavController,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService
  ) {}

  updateTask(action, data) {
    let comment: string;
    let userId: string;
    if (action === 'REASSIGN') {
      userId = data;
    } else if (action === 'APPROVE' || action === 'REJECT') {
      comment = data;
    }
    this.body = {
      tasks: [this.activatedRoute.snapshot.params.taskNumber],
      action: {
        id: action,
      },
      comment: {
        commentStr: comment,
        commentScope: 'TASK',
      },
      identities: [
        {
          id: userId,
          type: 'user',
        },
      ],
    };

    console.log(
      '__UPDATE__',
      action + ' ' + 'comment ' + comment + 'userId ' + userId
    );

    this.commonUiService.showLoader();
    this.apiService.updateTaskInformationById(this.body).subscribe(
      (res) => {
        console.log('res', res);
        this.commonUiService.hideLoader();

        if (isPlatform('capacitor')) {
          if (res && res.status && res.status === 400) {
            const msg =
              res && res.title
                ? res.title
                : this.translate.instant(
                    'TOSTER_MESSAGE.TRANSACTION_TOSTER.REJECT'
                  );

            this.commonUiService.presenErrortToast(msg);
          } else if (res && res.status && res.status !== 200) {
            const msg =
              res && res.title
                ? res.title
                : this.translate.instant(
                    'TOSTER_MESSAGE.TRANSACTION_TOSTER.REJECT'
                  );

            this.commonUiService.presenErrortToast(msg);
          } else {
            this.showToastNotification(action);
            // this.navController.back();
            this.router.navigate(['/tasks']);
          }
        } else {
          this.showToastNotification(action);
          // this.navController.back();
          this.router.navigate(['/tasks']);
        }
      },
      (error) => {
        console.log('error', error);
        if (error && error.status && error.status === 400) {
          const msg =
            error.error && error.error.title
              ? error.error.title
              : this.translate.instant(
                  'TOSTER_MESSAGE.TRANSACTION_TOSTER.REJECT'
                );

          this.commonUiService.presenErrortToast(msg);
        } else {
          const msg =
            error.error && error.error.title
              ? error.error.title
              : this.translate.instant(
                  'TOSTER_MESSAGE.TRANSACTION_TOSTER.REJECT'
                );

          this.commonUiService.presenErrortToast(msg);
        }
        this.commonUiService.hideLoader();
      }
    );
  }

  showToastNotification(actionSuccess) {
    let msg: string;
    enum action {
      'APPROVE' = 'approved',
      'REJECT' = 'rejected',
      'REASSIGN' = 'reassigned',
    }

    msg = `${this.translate.instant(
      'TOSTER_MESSAGE.MESSAGE.HEADING_SUCCESS_MESSAGE'
    )} ${this.convertApproveMessageFromTranslate(
      action[actionSuccess]
    )} ${this.translate.instant('TOSTER_MESSAGE.MESSAGE.SUCCESS')}  `;

    this.commonUiService.presentSuccessToast(msg);
  }

  /**
   *
   * @param alert
   * open the alert box showAlert
   */
  async showAlert(alert) {
    let taskCategory =
      this.activatedRoute.snapshot.params['category'] ===
      TASK_CATEGORIES.PURCHASING
        ? this.translate.instant('TASK_CATEGORIES.PURCHASING')
        : this.activatedRoute.snapshot.params['category'] ===
          TASK_CATEGORIES.PURCHASE_REQUISITIONS
        ? this.translate.instant('TASK_CATEGORIES.PURCHASE_REQUISITIONS')
        : this.translate.instant('TASK_CATEGORIES.INVOICE');
    if (alert === 'APPROVE') {
      await this.alertController
        .create({
          cssClass: 'my-custom-class',

          // message: `Approve the purchase order submitted by ${this.taskDetailsPage.tasks['AllApprovalDetails'][0]['Buyer']}?`,
          message: `${this.translate.instant('ALERT_MESSAGE.APPROVE_MESSAGE', {
            value: taskCategory,
          })}${this.taskDetailsPage.tasks[0].Buyer}?`,
          inputs: [
            {
              type: 'text',
              name: 'comment',

              placeholder: `${this.translate.instant(
                'ALERT_MESSAGE.PLACEHOLDER.MESSAGE'
              )} (${this.translate.instant(
                'ALERT_MESSAGE.PLACEHOLDER.OPTIONAL'
              )})`,
            },
          ],
          buttons: [
            {
              cssClass: 'approve-button',
              text: this.translate.instant('ALERT_MESSAGE.BUTTON_TEXT.APPROVE'),
              handler: (res) => {
                this.setComment(res.comment);
                this.updateTask('APPROVE', res.comment);
              },
            },
            {
              cssClass: 'cancel-button',
              text: this.translate.instant('ALERT_MESSAGE.BUTTON_TEXT.CANCEL'),
            },
          ],
        })
        .then((res) => res.present());
    } else if (alert === 'REJECT') {
      await this.alertController
        .create({
          cssClass: 'my-custom-class',

          // message: `Reject the purchase order submitted by ${this.taskDetailsPage.tasks['AllApprovalDetails'][0]['Buyer']}?`,
          message: `${this.translate.instant('ALERT_MESSAGE.REJECT_MESSAGE', {
            value: taskCategory,
          })} ${this.taskDetailsPage.tasks[0].Buyer}?`,
          inputs: [
            {
              type: 'text',
              name: 'comment',
              placeholder: 'Add note (optional)',
            },
          ],
          buttons: [
            {
              cssClass: 'reject-button',
              text: this.translate.instant('ALERT_MESSAGE.BUTTON_TEXT.REJECT'),
              handler: (res) => {
                this.setComment(res.comment);
                this.updateTask('REJECT', res.comment);
              },
            },
            {
              cssClass: 'cancel-button',
              text: this.translate.instant('ALERT_MESSAGE.BUTTON_TEXT.CANCEL'),
            },
          ],
        })
        .then((res) => res.present());
    } else if (alert === 'REASSIGN') {
      this.showForward();
    }
  }

  /**
   *
   * @param comment
   */
  setComment(comment) {
    this.apiService.setComment(comment);
  }
  /**
   *
   *  showForward modal when you click the forward button the function is working.
   */
  async showForward() {
    const modal = await this.modalController.create({
      component: ForwardTaskComponent,
      cssClass: 'my-custom-class',
      mode: 'ios'
    });
    modal.onDidDismiss().then((res) => {
      console.log('User data', res);
      if (res && res.data) {
        this.updateTask('REASSIGN', res.data);
      }
    });
    return await modal.present();
  }

  ngOnInit() {}

  /**
   * @return the success toster message using this function convertApproveMessageFromTranslate
   * @param message
   */

  convertApproveMessageFromTranslate(message) {
    switch (message) {
      case 'approved':
        return this.translate.instant(
          'TOSTER_MESSAGE.TRANSACTION_TOSTER.APPROVE'
        );
      case 'rejected':
        return this.translate.instant(
          'TOSTER_MESSAGE.TRANSACTION_TOSTER.REJECTED'
        );
      case 'reassigned':
        return this.translate.instant(
          'TOSTER_MESSAGE.TRANSACTION_TOSTER.REASSIGNED'
        );
    }
  }
}
