import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validator,
  FormBuilder,
  Validators,
} from '@angular/forms';
import {
  AlertController,
  LoadingController,
  NavController,
  PopoverController,
} from '@ionic/angular';
import { PopoverComponent } from 'src/app/components/popover/popover/popover.component';

import { ApiService } from '../../services/api-service/api.service';
import { AuthServiceProvider } from '../../services/auth-service/auth-service.service';
import { CommonUiService } from '../../services/common-ui/common-ui.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  formLogin: FormGroup;
  public list = [];
  hide: any = 'eye-off-outline';
  checkeyevalue: boolean = false;
  passwordType: any = 'password';
  constructor(
    public fb: FormBuilder,
    public alertController: AlertController,
    public navCtrl: NavController,
    private apiService: ApiService,
    private authService: AuthServiceProvider,
    private commonui: CommonUiService,

    private popCtrl: PopoverController
  ) {


    this.formLogin = new FormGroup({
      email: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.email],
      }),
      password: new FormControl(null, {
        updateOn: 'blur',
        validators: [Validators.required, Validators.minLength(8)],
      }),
    });
  }

  ngOnInit() { }
  // .......................login function //////////////////////////////////////////////////////////////
  async signIn() {
    /* const form = this.formLogin.value;

    const user = JSON.parse(localStorage.getItem('user'))

   if (user.name === form.name && user.password === form.password) {

      localStorage.setItem('Ingresado', 'true');
      this.navCtrl.navigateRoot('folder/Inbox')
    } else {
      const alert = await this.alertController.create({
        header: 'Wrong credentials',
        message: 'Email or password are incorrect.',
        buttons: ['Accept']
      });

      await alert.present();
      return;
    }*/

    // .......................................only validation part use here.............................//
    if (this.formLogin.valid) {
      this.commonui.showLoader();
      setTimeout(() => {
        this.commonui.hideLoader();
        this.navCtrl.navigateRoot('/tasks');
      }, 2000);
      await this.authService.setAuthStatus(true);
      return;
    } else {
      return;
    }
  }

  // end/////////////////

  // unhide password function///////////////////////////////////////


  /**
   * this function is unhide the passward
   * @function unhidepassword()
   */
  unhidepassword() {
    if (this.checkeyevalue) {
      this.hide = 'eye-off-outline';
      this.checkeyevalue = false;
      this.passwordType = 'password';
    } else {
      this.hide = 'eye-outline';
      this.checkeyevalue = true;
      this.passwordType = 'text';
    }
  }
  // end the unhide password function/////////////////////////////////

  /**
   * this function is use for toggle the language option
   * languageChooseOption()
   * @param ev
   */
  async languageChooseOption(ev) {
    const popover = await this.popCtrl.create({
      component: PopoverComponent,
      event: ev
    });
    await popover.present();
  }

}
