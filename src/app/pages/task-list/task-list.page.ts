import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api-service/api.service';
import * as _ from 'lodash';
import { TaskListModel } from '../../interfaces/task-list.model';
import { TABS_TYPE, TASK_CATEGORIES } from 'src/constants/constants';
import { SharedService } from '../../services/shared-services/shared-services.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.page.html',
  styleUrls: ['./task-list.page.scss'],
})
export class TaskListPage implements OnInit {
  contentLoaded = false;
  public list = [];
  public listTasksNumber = [];
  public listTasks = [];
  public CurrentTaskId = null;
  public CurrentTask = [];
  filterTerm: any;
  DataNotFoundMessage = this.translate.instant('NO_DATA.TASK');

  readonly TABS_TYPE = TABS_TYPE;
  @Output() onSendTask = new EventEmitter(null);

  constructor(
    private apiService: ApiService,
    private router: Router,
    private sharedservices: SharedService,
    private translate: TranslateService
  ) {
    //Call Users API for Forward
    this.apiService.getUsers();
  }

  dataList() {
    return this.apiService.getRequest().subscribe(
      (data) => {
        this.contentLoaded = true;
        if (data && data['items'] && data['items'].length > 0) {
          this.list = data['items'];
          let allList = data['items'];
          let allListData: TaskListModel[] = [];
          allListData = allList.map((m: any) => {
            const listMapping: TaskListModel = {
              Title: m.title, //Title
              TaskNumber: Number(m.number), //TaskNumber
              SubmitForApprDate: m.assignedDate,
              ActionPerformed: m.fromUserDisplayName,
              Category: m.category ? m.category : TASK_CATEGORIES.INVOICE,
              CategoryNumber: this.sharedservices.extractCategoryNumberFromTitle(m.category ? m.category : TASK_CATEGORIES.INVOICE, m.title)
              // categoryNumber store a number type as string and fuction helps to extractnumber form title
            };


            return listMapping;
          });
          this.listTasks = _.chain(allListData)
            .groupBy('TaskNumber')
            .map((value, key) => ({ TaskNumber: key, items: value }))
            .value();

          console.log('listTasks', this.listTasks);

        }
      },
      (err) => {
        this.contentLoaded = true;
        console.log('Error', err);
      }
    );
  }

  /**
   *
   * @param event
   * @param task
   */
  redirectToTaskById(event, task) {
    this.apiService.taskId = task.TaskNumber;
    let category = task.items[0]['Category'];
    this.router.navigate(['task-details', category, task.TaskNumber, 'information']);
  }

  getCurrentTaskDetail() {
    let taskId = 13231;
    return this.apiService.getTaskInformationById(taskId).subscribe(
      (data) => {
        if (
          data &&
          data['AllApprovalDetails'] &&
          data['AllApprovalDetails'].length > 0
        ) {
          console.log(data['AllApprovalDetails']);
          this.listTasks = _.chain(data['AllApprovalDetails'])
            .groupBy('TaskNumber')
            .map((value, key) => ({ TaskNumber: key, items: value }))
            .value();
        }
      },
      (error) => {
        console.log('Error', error);
      }
    );
  }

  ngOnInit() {

  }
  /**
   *
   * @param event
   * refresh the app (list page)
   */
  doRefresh(event) {
    console.log('Begin async operation');

    // this.getCurrentTaskDetail();
    this.dataList();
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  ionViewWillEnter() {
    this.dataList();
  }

  /**
   *
   * @param ev
   *
   *
   */
  fetchSearchInputValue(ev: any) {
    this.filterTerm = ev.target.value;
  }

}
