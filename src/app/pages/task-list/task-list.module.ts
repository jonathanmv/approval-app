import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TaskListPageRoutingModule } from './task-list-routing.module';

import { TaskListPage } from './task-list.page';

import { RouterModule } from '@angular/router';

import { Ng2SearchPipeModule } from 'ng2-search-filter';

import { SharedCompenentsModule } from '../../components/shared-component.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TaskListPageRoutingModule,
    RouterModule,
    Ng2SearchPipeModule,
    SharedCompenentsModule,
    TranslateModule
  ],
  exports: [],
  declarations: [
    TaskListPage,


  ],
})
export class TaskListPageModule { }
