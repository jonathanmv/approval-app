import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AttachmentsPageRoutingModule } from './attachments-routing.module';

import { AttachmentsPage } from './attachments.page';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { SharedCompenentsModule } from '../../components/shared-component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AttachmentsPageRoutingModule,
    SharedCompenentsModule,
  ],
  declarations: [AttachmentsPage],
  providers: [
    FileOpener,
  ]
})
export class AttachmentsPageModule { }
