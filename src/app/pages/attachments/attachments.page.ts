import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Directory, Filesystem } from '@capacitor/filesystem';
import { ApiService } from '../../services/api-service/api.service';

import { FileOpener } from '@ionic-native/file-opener/ngx';
import { CommonUiService } from '../../services/common-ui/common-ui.service';
import { isPlatform } from '@ionic/core';
import { TABS_TYPE } from 'src/constants/constants';
import { TaskDetailsDataService } from 'src/app/services/task-details-data/task-details-data.service';
@Component({
  selector: 'app-attachments',
  templateUrl: './attachments.page.html',
  styleUrls: ['./attachments.page.scss'],
})
export class AttachmentsPage implements OnInit {
  public attachments: any;
  canload = false;
  downloadUrl = '';
  myFiles = [];
  downloadProgress = 0;
  readonly TABS_TYPE = TABS_TYPE;
  taskInformation: any;
  constructor(
    private apiService: ApiService,
    private fileOpener: FileOpener,
    private commonUiService: CommonUiService,
    private detailService: TaskDetailsDataService,
  ) { }

  getAttachments(taskId) {
    this.apiService.getAttachments(taskId).subscribe(
      (data) => {
        this.canload = true;
        this.attachments = data['items'];
        console.log(this.attachments);
      },
      (error) => {
        this.canload = true;
      }
    );
  }

  ngOnInit() {
    this.getAttachments(this.detailService.getCurrentTaskNumber());
  }

  // Helper functions
  private convertBlobToBase64 = (blob: Blob) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onerror = reject;
      reader.onload = () => {
        resolve(reader.result);
      };
      reader.readAsDataURL(blob);
    });

  private getMimetype(name) {
    if (name.indexOf('pdf') >= 0) {
      return 'application/pdf';
    } else if (name.indexOf('png') >= 0) {
      return 'image/png';
    } else if (name.indexOf('mp4') >= 0) {
      return 'video/mp4';
    }
  }


  /**
   *
   * @param title
   * @param url
   * download the atachment
   */
  downloadFile(title, url?) {
    console.log('URL', url);
    // To use our dummy URLs
    this.downloadUrl = url ? url : this.downloadUrl;
    const headers = new HttpHeaders().set(
      'Authorization',
      'Basic UkFKLmNsZDE6d2VsY29tZTEyMw=='
    );
    this.commonUiService.showLoader();

    this.apiService.downloadAttachments(this.downloadUrl).subscribe(
      async (event) => {
        console.log('--event--', event);
        if (isPlatform('capacitor')) {
          //TODO: Write code for mobile app
          this.writeFile(title, 'data:application/pdf;base64,' + event);
        } else {
          if (event.type === HttpEventType.DownloadProgress) {
            this.downloadProgress = Math.round(
              (100 * event.loaded) / event.total
            );
          } else if (event.type === HttpEventType.Response) {
            this.writeFile(title, event.body);
          }
        }
      },
      (error) => {
        this.commonUiService.hideLoader();
      }
    );
  }

  /**
   *
   * @param title
   * @param body
   */
  async writeFile(title, body) {
    this.commonUiService.hideLoader();
    this.downloadProgress = 0;

    // const name = this.downloadUrl.substr(this.downloadUrl.lastIndexOf('/') + 1);
    const name = title;
    console.log('--name--', name);

    // const base64 = await this.convertBlobToBase64(body) as string;
    // console.log("--base64--", base64);

    let base64 = body;
    if (!isPlatform('capacitor')) {
      base64 = (await this.convertBlobToBase64(body)) as string;
    }
    console.log('--base64--', base64);

    const options = {
      path: name,
      data: base64,
      // directory: FilesystemDirectory.Data,
      directory: Directory.Data,
    };
    console.log('--options--', options);

    const savedFile1 = await Filesystem.getUri(options);
    console.log('--savedFile1--', savedFile1);

    const savedFile = await Filesystem.writeFile(options);
    console.log('--savedFile--', savedFile);

    const path = savedFile.uri;
    // const path = savedFile1.uri;
    console.log('--path--', path);

    const mimeType = this.getMimetype(name);
    console.log('--mimeType--', mimeType);

    // this.fileOpener.open(path, mimeType)
    //   .then(() => console.log('File is opened'))
    //   .catch(error => console.log('Error openening file', error));

    this.fileOpener
      .showOpenWithDialog(path, mimeType)
      .then(() => console.log('File is opened'))
      .catch((error) => console.log('Error openening file', error));

    this.myFiles.unshift(path);
  }
}
