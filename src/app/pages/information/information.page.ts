import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { TaskDetailsDataService } from '../../services/task-details-data/task-details-data.service';
import { TASK_CATEGORIES } from 'src/constants/constants';

@Component({
  selector: 'app-information',
  templateUrl: './information.page.html',
  styleUrls: ['./information.page.scss'],
})
export class InformationPage implements OnInit {
  private taskInformation: any;
  private category: any;
  @ViewChild(IonContent, { static: false }) content: IonContent;
  readonly TASK_CATEGORIES = TASK_CATEGORIES;

  constructor(
    private detailService: TaskDetailsDataService,
  ) { }

  ngOnInit() {
    this.taskInformation = this.detailService.getCurrentTaskInfo();
    this.category = this.detailService.getCurrentTaskCategory();
  }
}
