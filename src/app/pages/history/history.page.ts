import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api-service/api.service';
import * as _ from 'lodash';
import { TaskDetailsDataService } from '../../services/task-details-data/task-details-data.service';
import { TABS_TYPE, TASK_CATEGORIES } from 'src/constants/constants';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  canload = false;
  public history: any;
  currentTask: any;
  category: any;
  readonly TABS_TYPE = TABS_TYPE;
  readonly TASK_CATEGORIES = TASK_CATEGORIES;

  constructor(
    private apiService: ApiService,
    private detailService: TaskDetailsDataService,
  ) { }

  /**
   *
   * @param taskId
   * get the history data
   * @function getHistory
   */
  getHistory(taskId) {
    this.apiService.getTaskHistory(taskId).subscribe(
      (data) => {
        this.canload = true;
        this.history = _.sortBy(data['items'], 'updatedDate').reverse();
        console.log(this.history);
      },
      (error) => {
        this.canload = true;
      }
    );
  }

  ngOnInit() {
    this.category = this.detailService.getCurrentTaskCategory();
    this.currentTask = this.detailService.getCurrentTaskInfo();
    this.getHistory(this.detailService.getCurrentTaskNumber());
  }
}
