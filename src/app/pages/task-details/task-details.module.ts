import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TaskDetailsPageRoutingModule } from './task-details-routing.module';
import { TaskDetailsPage } from './task-details.page';
import { InformationPage as InformationComponent } from '../information/information.page';

import { ActionButtonsComponent } from '../action-buttons/action-buttons.component';

import { SharedCompenentsModule } from '../../components/shared-component.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TaskDetailsPageRoutingModule,
    SharedCompenentsModule,
    TranslateModule
  ],
  exports:[],
  declarations: [
    TaskDetailsPage,

    ActionButtonsComponent,

    InformationComponent,
  ]
})
export class TaskDetailsPageModule { }
