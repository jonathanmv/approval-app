import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonContent } from '@ionic/angular';
import { TABS_TYPE, TASK_CATEGORIES } from 'src/constants/constants';
import { ApiService } from '../../services/api-service/api.service';
import { TaskDetailsDataService } from '../../services/task-details-data/task-details-data.service';

@Component({
  providers: [],
  selector: 'task-details',
  templateUrl: './task-details.page.html',
  styleUrls: ['./task-details.page.scss'],
})
export class TaskDetailsPage implements OnInit {
  @Input() task: [];
  @ViewChild(IonContent, { static: false }) content: IonContent;
  public tasks: {};
  readonly TABS_TYPE = TABS_TYPE;
  readonly TASK_CATEGORIES = TASK_CATEGORIES;

  private taskInformation: any;
  canload: boolean = false;
  private category: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    public apiService: ApiService,
    private detailService: TaskDetailsDataService
  ) { }

  ngOnInit() {
    let taskNumber = this.activatedRoute.snapshot.params['taskNumber'];
    let category = this.activatedRoute.snapshot.params['category'];
    //Setting Current Task data
    this.detailService.setCurrentTaskNumber(taskNumber);
    this.detailService.setCurrentTaskCategory(category);
    this.apiService
      .getTaskInformationById(taskNumber)
      .subscribe(
        (data) => {
          this.canload = true;
          if (
            data &&
            data['AllApprovalDetails'] &&
            data['AllApprovalDetails'].length > 0
          ) {
            this.tasks = data['AllApprovalDetails'];
            this.detailService.setCurrentTaskInfo(this.tasks);


          }
        },
        (error) => {
          this.canload = true;
          console.log('error', error);
        }
      );
  }
}
