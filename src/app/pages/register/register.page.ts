import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validator,
  FormBuilder,
  Validators
} from '@angular/forms'
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  formRegister: FormGroup;

  constructor(
    public fb: FormBuilder,
    public alertController: AlertController,
    public navCtrl: NavController,
  ) {
    this.formRegister = this.fb.group({
      'name': new FormControl("", Validators.required),
      'password': new FormControl("", Validators.required),
      'confirmationPassword': new FormControl("", Validators.required),
    })
  }

  ngOnInit() {
  }

  async save() {
    const form = this.formRegister.value;

    if (this.formRegister.invalid) {
      const alert = await this.alertController.create({
        header: 'Datos incompletos',
        message: 'Tienes que llenar todos los datos.',
        buttons: ['Accept']
      });

      await alert.present();
      return;
    }

    const user = {
      name: form.name,
      password: form.password
    }

    localStorage.setItem('user', JSON.stringify(user));

    localStorage.setItem('Ingresado', 'true');
    this.navCtrl.navigateRoot('folder/Inbox')
  }

}
