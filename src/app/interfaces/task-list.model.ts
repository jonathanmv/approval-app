export interface TaskListModel {
  Category?: string;
  VendorName?: string;
  TaskNumber?: number;
  ActionPerformed?: string;
  SubmitForApprDate?: Date;
  Title?: string;
  CategoryNumber?: string;

}
