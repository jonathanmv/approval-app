import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthServiceGuard } from './guards/auth-service/auth-service.guard';
import { AutoLoginGuard } from './guards/auto-login/auto-login.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full',
  },
  {
    path: 'welcome',
    loadChildren: () =>
      import('./pages/welcomepage/welcomepage/welcomepage.module').then(
        (m) => m.WelcomepagePageModule
      ),
    canActivate: [AutoLoginGuard]
  },
  {
    path: 'task-details/:category/:taskNumber',
    loadChildren: () =>
      import('./pages/task-details/task-details.module').then(
        (m) => m.TaskDetailsPageModule
      ),
    canActivate: [AuthServiceGuard],
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginPageModule),
    canActivate: [AutoLoginGuard]
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./pages/register/register.module').then((m) => m.RegisterPageModule),
  },
  {
    path: 'attachments',
    loadChildren: () =>
      import('./pages/attachments/attachments.module').then(
        (m) => m.AttachmentsPageModule
      ),
    canActivate: [AuthServiceGuard],
  },
  {
    path: 'tasks',
    loadChildren: () =>
      import('./pages/task-list/task-list.module').then((m) => m.TaskListPageModule),
    canActivate: [AuthServiceGuard],
  },
  {
    path: 'history',
    loadChildren: () =>
      import('./pages/history/history.module').then((m) => m.HistoryPageModule),
    canActivate: [AuthServiceGuard],
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
